package com.test.movielibrary.common

object Constants {
    const val AUTH = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkOTcyNzFlNjJiMWRhOGZlZjE4MWQ3NjAxM2IwMjE2MSIsInN1YiI6IjY0ZTcyZjU1YzNjODkxMDBlMzVmOWY4OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.7JODBS3pQnhlFMxomOpcFpfh61BLlw40QEGbcmL013k"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/original/"

    const val GENRE_MOVIE = "genre/movie/list"
    const val DISCOVER = "discover/movie"
}