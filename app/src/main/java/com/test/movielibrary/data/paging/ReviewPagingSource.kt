package com.test.movielibrary.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.test.movielibrary.data.remote.reviews.UserReview
import com.test.movielibrary.domain.repository.NetworkRepository
import retrofit2.HttpException
import java.io.IOException

class ReviewPagingSource(
    private val networkRepository: NetworkRepository,
    private val moviesId: Int
) : PagingSource<Int, UserReview>() {
    override fun getRefreshKey(state: PagingState<Int, UserReview>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UserReview> {
        val currentPage = params.key ?: 1
        return try {
            val response = networkRepository.getReviews(page = currentPage, movieId = moviesId)
            LoadResult.Page(
                data = response.results,
                prevKey = null,
                nextKey = response.page.plus(1)
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }
}