package com.test.movielibrary.data.remote

import com.test.movielibrary.common.Constants
import com.test.movielibrary.data.remote.genre.GenreResponse
import com.test.movielibrary.data.remote.movie.MovieResponse
import com.test.movielibrary.data.remote.movie_detail.MovieDetailDto
import com.test.movielibrary.data.remote.reviews.ReviewResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET(Constants.GENRE_MOVIE)
    suspend fun getGenres(
        @Header("Authorization") authorization: String = Constants.AUTH,
        @Query("language") language: String = "en"
    ): GenreResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: Int,
        @Header("Authorization") authorization: String = Constants.AUTH,
        @Query("language") language: String = "en",
        @Query("append_to_response") appendToResponse: String = "credits,similar"
    ): MovieDetailDto

    @GET(Constants.DISCOVER)
    suspend fun getMovieWithGenres(
        @Header("Authorization") authorization: String = Constants.AUTH,
        @Query("language") language: String = "en",
        @Query("sort_by") sortBy: String = "popularity.desc",
        @Query("page") page: Int,
        @Query("with_genres") genreId: Int,
    ): MovieResponse

    @GET("movie/{movie_id}/reviews")
    suspend fun getMovieReviews(
        @Path("movie_id") movieId: Int,
        @Header("Authorization") authorization: String = Constants.AUTH,
        @Query("language") language: String = "en",
        @Query("page") page: Int
    ): ReviewResponse
}