package com.test.movielibrary.data.remote.reviews

import com.google.gson.annotations.SerializedName

data class AuthorDetails(
    val name: String,
    val username: String,
    @SerializedName("avatar_path")
    val avatarPath: String?,
    val rating: String?
)
