package com.test.movielibrary.data.remote.reviews

import com.google.gson.annotations.SerializedName


data class ReviewResponse(
    val page: Int,
    val results: List<UserReview>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int,
)
