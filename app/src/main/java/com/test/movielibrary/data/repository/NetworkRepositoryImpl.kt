package com.test.movielibrary.data.repository

import com.test.movielibrary.common.Resource
import com.test.movielibrary.data.remote.ApiService
import com.test.movielibrary.data.remote.genre.GenreResponse
import com.test.movielibrary.data.remote.movie.MovieResponse
import com.test.movielibrary.data.remote.movie_detail.toMovieDetail
import com.test.movielibrary.data.remote.reviews.ReviewResponse
import com.test.movielibrary.domain.model.MovieDetail
import com.test.movielibrary.domain.repository.NetworkRepository
import com.test.movielibrary.presentation.navigation.Screen
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor(
    private val api: ApiService,
) : NetworkRepository {

    override suspend fun getGenres(): GenreResponse {
        return api.getGenres()
    }

    override suspend fun getMovieWithGenres(page: Int, genreId: Int): MovieResponse {
        return api.getMovieWithGenres(page = page, genreId = genreId)
    }

    override suspend fun getReviews(page: Int, movieId: Int): ReviewResponse {
        return api.getMovieReviews(movieId = movieId, page= page)
    }

    override fun getMovieById(id: Int): Flow<Resource<MovieDetail>> = flow {
        emit(Resource.Loading())
        try {
            val movie = api.getMovieDetail(movieId = id).toMovieDetail()
            emit(Resource.Success(movie))
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage  ?: "Error"))
        }
    }

}




