package com.test.movielibrary.domain.repository

import com.test.movielibrary.common.Resource
import com.test.movielibrary.data.remote.genre.GenreResponse
import com.test.movielibrary.data.remote.movie.MovieResponse
import com.test.movielibrary.data.remote.reviews.ReviewResponse
import com.test.movielibrary.domain.model.MovieDetail
import kotlinx.coroutines.flow.Flow


interface NetworkRepository {

    suspend fun getGenres(): GenreResponse

    fun getMovieById(id: Int): Flow<Resource<MovieDetail>>

    suspend fun getMovieWithGenres(page: Int, genreId: Int): MovieResponse

    suspend fun getReviews(page: Int, movieId: Int): ReviewResponse
}