package com.test.movielibrary.presentation.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.StarRate
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.skydoves.landscapist.CircularReveal
import com.skydoves.landscapist.ShimmerParams
import com.skydoves.landscapist.coil.CoilImage
import com.test.movielibrary.common.Constants
import com.test.movielibrary.presentation.detail.components.MovieCast
import com.test.movielibrary.presentation.detail.components.MovieDescription
import com.test.movielibrary.presentation.detail.components.MovieGenres
import com.test.movielibrary.presentation.detail.components.SimilarMovies
import com.test.movielibrary.presentation.navigation.Screen
import com.test.movielibrary.ui.theme.ratingStarColor
import kotlin.time.Duration.Companion.minutes

@Composable
fun MovieDetailScreen(
    viewModel: MovieDetailViewModel = hiltViewModel(),
    navController: NavController,
    ) {
    val state = viewModel.state.value


    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.surface)
            .verticalScroll(rememberScrollState())
            .navigationBarsPadding()
    ) {
        state.movie?.let { movie ->

            val genres = remember {
                mutableStateOf(movie.genres)
            }

            val (backdrop, poster, arrowBack, _, originalTitle) = createRefs()
            val (rateScore, genresFlowRow, row, description, similar, cast) = createRefs()


            CoilImage(
                imageModel = Constants.IMAGE_URL + movie.backdropPath,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(350.dp)
                    .clip(RoundedCornerShape(bottomStart = 32.dp, bottomEnd = 32.dp))
                    .constrainAs(backdrop) {},
                contentScale = ContentScale.FillHeight,
                shimmerParams = ShimmerParams(
                    baseColor = MaterialTheme.colors.background,
                    highlightColor = Color.LightGray.copy(alpha = 0.6f),
                    durationMillis = 350, dropOff = 0.65f, tilt = 20f
                ),
                circularReveal = CircularReveal(duration = 350),
                failure = { Text(text = "Failed to Load") },
            )

            CoilImage(
                imageModel = Constants.IMAGE_URL + movie.posterPath,
                modifier = Modifier
                    .width(150.dp)
                    .height(180.dp)
                    .clip(RoundedCornerShape(24.dp))
                    .constrainAs(poster) {
                        centerAround(backdrop.bottom)
                        linkTo(start = parent.start, end = parent.end)
                    },
                circularReveal = CircularReveal(duration = 350),
            )
            IconButton(
                onClick = { navController.popBackStack() },
                modifier = Modifier.constrainAs(arrowBack) {
                    linkTo(start = parent.start, end = poster.start)
                    linkTo(top = backdrop.bottom, bottom = poster.bottom)
                }
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Arrow Back Icon"
                )
            }

            Text(
                text = movie.originalTitle,
                modifier = Modifier.constrainAs(originalTitle) {
                    linkTo(start = parent.start, end = parent.end)
                    top.linkTo(poster.bottom, margin = 4.dp)
                }
            )
            Row(
                modifier = Modifier
                    .clickable{
                        navController.navigate(Screen.MovieReviews.route + "/${movie.id}/${movie.title}")
                    }
                    .constrainAs(rateScore) {
                    top.linkTo(originalTitle.bottom, margin = 4.dp)
                    linkTo(start = parent.start, end = parent.end)
                },
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    imageVector = Icons.Filled.StarRate, contentDescription = "",
                    tint = ratingStarColor
                )
                Text(text = "${movie.voteAverage} / 10 IMDb", color = Color.LightGray)
            }

            MovieGenres(
                genres = genres.value,
                modifier = Modifier.constrainAs(genresFlowRow) {
                    linkTo(start = parent.start, end = parent.end)
                    top.linkTo(rateScore.bottom, margin = 4.dp)
                }, navController = navController
            )
            Row(
                modifier = Modifier
                    .constrainAs(row) {
                        top.linkTo(genresFlowRow.bottom, margin = 8.dp)
                    }
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(text = "Time", color = Color.LightGray, fontSize = 16.sp)
                    Text(text = "${movie.runtime?.minutes}")
                }
                Column {
                    Text(text = "Language", color = Color.LightGray, fontSize = 16.sp)
                    Text(text = movie.spokenLanguages[0].englishName)
                }
                Column {
                    Text(text = "Status", color = Color.LightGray, fontSize = 16.sp)
                    Text(text = movie.status)
                }

            }
            MovieDescription(
                description = "${movie.overview}",
                modifier = Modifier.constrainAs(description) {
                    top.linkTo(row.bottom, margin = 8.dp)
                    linkTo(start = parent.start, end = parent.end)
                }
            )
            MovieCast(
                cast = movie.credit.cast,
                modifier = Modifier.constrainAs(cast) {
                    top.linkTo(description.bottom, margin = 8.dp)
                    linkTo(start = parent.start, end = parent.end)
                }
            )
            SimilarMovies(
                similar = movie.similar.results,
                modifier = Modifier.constrainAs(similar) {
                    top.linkTo(cast.bottom, margin = 8.dp)
                    linkTo(start = parent.start, end = parent.end)
                }
            ) { similarMovie ->
                navController.navigate(Screen.MovieDetail.route + "/${similarMovie.id}")
            }

        }
        val (circularProgress, errorString) = createRefs()
        if (state.isLoading) {
            CircularProgressIndicator(
                modifier = Modifier.constrainAs(circularProgress) {
                    linkTo(start = parent.start, end = parent.end)
                    linkTo(top = parent.top, bottom = parent.bottom)
                },
                color = Color.DarkGray
            )
        }
        if (state.error.isNotBlank()) {
            Text(text = state.error,
                modifier = Modifier.constrainAs(errorString) {
                    linkTo(start = parent.start, end = parent.end)
                    linkTo(top = parent.top, bottom = parent.bottom)
                })
        }
    }
}
