package com.test.movielibrary.presentation.detail

import com.test.movielibrary.data.local.entities.MovieEntity


sealed class MoviesEvent {
    data class BookmarkMovie(val movie: MovieEntity) : MoviesEvent()
    data class DeleteMovie(val movie: MovieEntity): MoviesEvent()
    data class IsBookmarked(val id: Int): MoviesEvent()
}