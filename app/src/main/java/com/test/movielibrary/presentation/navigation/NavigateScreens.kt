package com.test.movielibrary.presentation.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.test.movielibrary.presentation.detail.MovieDetailScreen
import com.test.movielibrary.presentation.genres.GenresScreen
import com.test.movielibrary.presentation.movie_genres.MovieWithGenres
import com.test.movielibrary.presentation.reviews.ReviewsScreen

@Composable
fun NavigateScreens(
    navController: NavHostController,
    paddingValues: PaddingValues
) {
    NavHost(
        navController = navController, startDestination = Screen.Genres.route,
        modifier = Modifier.padding(paddingValues)
    ) {
        composable(Screen.MovieDetail.route + "/{movie_id}") {
            MovieDetailScreen(navController = navController)
        }
        composable(Screen.Genres.route) {
            GenresScreen(navController = navController)
        }
        composable(
            Screen.MovieWithGenres.route + "/{genreId}/{genreName}",
            arguments = listOf(
                navArgument("genreId") { type = NavType.IntType },
                navArgument("genreName") { type = NavType.StringType }
            )
        ) { backStackEntry ->
            val genreId = backStackEntry.arguments?.getInt("genreId")
            val genreName = backStackEntry.arguments?.getString("genreName")
            MovieWithGenres(navController = navController, genreId = genreId, genreName = genreName)
        }
        composable(
            Screen.MovieReviews.route + "/{movieId}/{movieTitle}",
            arguments = listOf(
                navArgument("movieId") {type = NavType.IntType},
                navArgument("movieTitle") {type = NavType.StringType}
            )
        ){ backStackEntry ->
            val movieId = backStackEntry.arguments?.getInt("movieId")
            val movieTitle = backStackEntry.arguments?.getString("movieTitle")
            ReviewsScreen(movieId = movieId, movieTitle = movieTitle)
        }
    }
}