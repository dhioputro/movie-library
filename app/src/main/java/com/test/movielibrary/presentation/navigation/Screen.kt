package com.test.movielibrary.presentation.navigation

sealed class Screen(val route: String){
    object MovieDetail: Screen(route = "movie_detail")
    object Genres: Screen(route = "genres")
    object MovieWithGenres: Screen(route = "movie_with_genres")
    object MovieReviews: Screen(route = "movie_reviews")
}
