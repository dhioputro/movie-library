package com.test.movielibrary.presentation.reviews

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.test.movielibrary.data.paging.ReviewPagingSource
import com.test.movielibrary.data.remote.reviews.UserReview
import com.test.movielibrary.domain.repository.NetworkRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class ReviewsViewModel @Inject constructor(
    private val networkRepository: NetworkRepository,
): ViewModel() {
    fun userReviews(movieId: Int): Flow<PagingData<UserReview>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = {
                ReviewPagingSource(
                    networkRepository,
                    movieId
                )
            }
        ).flow.cachedIn(viewModelScope)
    }
}